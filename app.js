require('dotenv').config()
const crypto = require('crypto')
const cors = require('cors')
const express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')

const instance = axios.create({
  baseURL: process.env.API_URL,
  headers: {
    'Content-Type': 'application/json',
    'Auth-Token': process.env.AUTH_TOKEN,
    'App-Token': process.env.APP_TOKEN
  }
})

const app = express()
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())
const api = process.env.API_URL
const hashdata = crypto.createHash('md5').update("data").digest("hex")

app.get(`/${hashdata}`, async (req, res) => {
  const organization_memberships = true || req.query.organization_memberships
  const project_memberships = true || req.query.project_memberships
  try {
    const { data: {organizations} } = await instance.get(`/organizations`)
    const {id} = organizations.filter(x => x.name === 'You Are a CEO, Inc.').reduce(x => x.id)
    const { data: {users} } = await instance.get(`/users?organization_memberships=${organization_memberships}&project_memberships=${project_memberships}`)
    const filterUsers = users.map(user => {
      user.organizations = user.organizations.filter(x => x.id === id)
      return user
    })
    const opscaleUsers = filterUsers.filter(user => user.email.includes("opscale.io"))
    const bseUsers = filterUsers.filter(user => user.email.includes("bsetxt.com"))
    const { data: { organizations: timeData } } = await instance.get(`/weekly/team?organizations=${id}`)
      const returnDataOpscale = []
      const bsetxtUsers = []
      timeData.forEach(temp => {
        temp.users.forEach(user1 => {
          opscaleUsers.forEach(user2 => {
            if(user1.id === user2.id) {
              const newUser = user2
              newUser.duration = user1.duration
              returnDataOpscale.push(newUser)
            }
          })
          bseUsers.forEach(user2 => {
            if(user1.id === user2.id) {
              const newUser = user2
              newUser.duration = user1.duration
              bsetxtUsers.push(newUser)
            }
          })
        })
      })
      return res.send({
        opscaleUsers: returnDataOpscale,
        bsetxtUsers: bsetxtUsers
      })
  } catch (e) {
    console.log(e);
  }
})

app.listen(process.env.PORT, (err) => {
  if(err) throw err
  console.log(`running on http://localhost:${process.env.PORT}`);
})
