FROM node:8

WORKDIR /usr/src/hubstaffapi

COPY package*.json ./
RUN npm install
COPY ./package*.json ./
RUN npm install

WORKDIR /usr/src/hubstaffapi

COPY . .

EXPOSE 8081

CMD ["npm", "start"]
